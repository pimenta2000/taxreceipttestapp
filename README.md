# TAX RECEIPT TEST APP (Liferay Test)

This app implementations for the following services:


Basic sales tax is applicable at a rate of 10% on all goods, except books, food, and medical products that are exempt. Import duty is an additional sales tax applicable on all imported goods at a rate of 5%, with no exemptions.  When I purchase items, I receive a receipt which lists the name of all the items and their price (including tax), finishing with the total cost of the items, and the total amounts of sales taxes paid. The rounding rules for sales tax are that for a tax rate of n%, a shelf price of p contains (np/100 rounded up to the nearest 0.05) amount of sales taxx. 

This project was built with Spring Boot, available [here](http://projects.spring.io/spring-boot/).

According Spring Boot site, Spring Boot makes it easy to create stand-alone, production-grade Spring based Applications that you can "just run". We take an opinionated view of the Spring platform and third-party libraries so you can get started with minimum fuss. Most Spring Boot applications need very little Spring configuration.

<br>

## Getting Started

  1. JDK 1.8 or later
  2. A favorite text editor or IDE
  3. Download the source repository for this guide, or clone it using git clone https://pimenta2000@bitbucket.org/pimenta2000/taxreceipttestapp.git
  4. Import Maven Project in your IDE or package it
  5. Run JUnit tests in src.main.test package
  6. Start App 
  	- you can run Applicationjava in src.br.taxReceipt.controller
  	- you can run the application using mvn spring-boot:run. 
  	- you can build the JAR file with mvn clean package and run the JAR by typing:	java -jar target/taxReceipt-web-content-0.1.0.jar
  	



## Main Page

The main page for all the services can be accessed on the following URL:

  * http://localhost:8080/

