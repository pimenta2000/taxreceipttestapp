package br.taxReceipt.model;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

public class Product {
	private String description;
	private int amount;
	private double price;
	private boolean isImported = false;
	private final int importDuty = 5;
	protected int salesTax;
	
	public Product(){
		this.salesTax = 10;
	}
	
	public Product(String description, int amount, double price) {
		this();
		this.description = description;
		this.amount = amount;
		this.price = price;
	}
	
	public Product(String description, int amount, double price, boolean isImported) {
		this(description, amount, price);
		this.isImported = isImported;
	}	
	
	public int getAmount() {
		return amount;
	}

	public void setAmount(int amount) {
		this.amount = amount;
	}

	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public boolean isImported() {
		return isImported;
	}

	public void setImported(boolean isImported) {
		this.isImported = isImported;
	}

	public int getImportDuty() {
		return importDuty;
	}

	public double getSalesPrice(){
		return this.price + getSalesTaxValue();
	}
	
	public String getPriceFormatted(){
		return new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.US)).format(getPrice());
	}
	
	public String getSalesPriceFormatted(){
		return new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.US)).format(getSalesPrice());
	}
	
	public double getSalesTaxValue(){
		double totalTax = isImported ? this.salesTax + importDuty : salesTax;
		BigDecimal taxValue = new BigDecimal(this.price * (totalTax/100));
		BigDecimal result =  new BigDecimal(Math.ceil(taxValue.doubleValue() * 20) / 20);
		return result.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
}
