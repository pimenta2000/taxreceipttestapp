package br.taxReceipt.model;


public class Book extends NoTaxProduct{

	public Book(String description, int amount, double price) {
		super(description, amount, price);
	}
	
	public Book(String description, int amount, double price, boolean isImported) {
		super(description, amount, price, isImported);
	}		
	
}
