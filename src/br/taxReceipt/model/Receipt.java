package br.taxReceipt.model;

import java.util.ArrayList;
import java.util.List;

public class Receipt {
	private int inputId;
	private List<Product> products = new ArrayList<Product>();
	public int getInputId() {
		return inputId;
	}
	public void setInputId(int inputId) {
		this.inputId = inputId;
	}
	public List<Product> getProducts() {
		return products;
	}
	public void setProducts(List<Product> products) {
		this.products = products;
	}

	
}
