package br.taxReceipt.model;


public class MedicalProduct extends NoTaxProduct{

	public MedicalProduct(String description, int amount, double price) {
		super(description, amount, price);
	}
	
	public MedicalProduct(String description, int amount, double price, boolean isImported) {
		super(description, amount, price, isImported);
	}		
	
}
