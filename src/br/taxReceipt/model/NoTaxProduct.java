package br.taxReceipt.model;

import java.math.BigDecimal;

public class NoTaxProduct extends Product{
	
	public NoTaxProduct(String description, int amount, double price) {
		super(description, amount, price);
		super.salesTax = 0;
	}
	
	public NoTaxProduct(String description, int amount, double price, boolean isImported) {
		super(description, amount, price, isImported);
	}		

	//TODO Rever
	public double getSalesTaxValue(){
		double tax = super.isImported() ? (double)super.getImportDuty()/100 : 0;
		BigDecimal taxValue = new BigDecimal(super.getPrice() * tax);
		BigDecimal result =  new BigDecimal(Math.ceil(taxValue.doubleValue() * 20) / 20);
		return result.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();
	}
	
}
