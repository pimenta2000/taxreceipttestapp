package br.taxReceipt.model;

public class Food extends NoTaxProduct{
	public Food(String description, int amount, double price) {
		super(description, amount, price);
	}
	
	public Food(String description, int amount, double price, boolean isImported) {
		super(description, amount, price, isImported);
	}	

}
