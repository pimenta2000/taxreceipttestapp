package br.taxReceipt.dao;

import br.taxReceipt.bo.ShoppingBasket;

public interface Shopping {
	ShoppingBasket find(int inputId);
}
