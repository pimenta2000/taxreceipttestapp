package br.taxReceipt.dao;

import java.util.HashMap;
import java.util.Map;

import br.taxReceipt.bo.ShoppingBasket;
import br.taxReceipt.model.Book;
import br.taxReceipt.model.Food;
import br.taxReceipt.model.MedicalProduct;
import br.taxReceipt.model.Product;

public class ShoppingImpl implements Shopping {
	
	private Map<Integer, ShoppingBasket> baskets = new HashMap<Integer, ShoppingBasket>();
	private static boolean IMPORTED_PRODUCT = true;
	
	public ShoppingImpl(){
		ShoppingBasket basket = new ShoppingBasket();
		basket.setInputId(1);
		basket.insert(new Product("music cd", 1, 14.99));
		basket.insert(new Book("book", 1, 12.49));
		basket.insert(new Food("chocolate bar", 1, 0.85));
		baskets.put(1, basket);
		
		basket = new ShoppingBasket();
		basket.setInputId(2);
		basket.insert(new Food("imported box of chocolates", 1, 10.00, IMPORTED_PRODUCT));
		basket.insert(new Product("imported bottle of perfume", 1, 47.50, IMPORTED_PRODUCT));
		baskets.put(2, basket);
		
		basket = new ShoppingBasket();
		basket.setInputId(3);
		basket.insert(new Product("imported bottle of perfume", 1, 27.99, IMPORTED_PRODUCT));
		basket.insert(new Product("bottle of perfume", 1, 18.99));
		basket.insert(new MedicalProduct("packet of headache pills", 1, 9.75));
		basket.insert(new Food("imported box of chocolates", 1, 11.25, IMPORTED_PRODUCT));
		baskets.put(3, basket);
	}

	@Override
	public ShoppingBasket find(int inputId) {
		return (ShoppingBasket) baskets.get(inputId);
	}

}
