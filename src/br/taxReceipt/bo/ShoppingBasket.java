package br.taxReceipt.bo;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

import br.taxReceipt.model.Product;
import br.taxReceipt.model.Receipt;

public class ShoppingBasket {
	private Receipt receipt = new Receipt();
	
	public void setInputId(int id){
		receipt.setInputId(id);
	}
	
	public void insert(Product product) {
		receipt.getProducts().add(product);
	}
	
	public List<Product> getProducts(){
		return receipt.getProducts();
	}
	
	public double getTotalTaxValue(){
		double totalTaxValue = 0;
		for (Product product : receipt.getProducts()) {
			totalTaxValue += product.getSalesTaxValue();
		}
		return totalTaxValue;
	}
	
	public String getTotalTaxValueFormated(){
		return new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.US)).format(getTotalTaxValue());
	}
	
	public double getTotalValue(){
		double totalTaxValue = 0;
		for (Product product : receipt.getProducts()) {
			totalTaxValue += product.getSalesPrice();
		}
		return totalTaxValue;
	}

	public String getTotalValueFormated(){
		return new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.US)).format(getTotalValue());
	}
	
	
}
