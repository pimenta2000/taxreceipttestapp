package br.taxReceipt.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import br.taxReceipt.bo.ShoppingBasket;
import br.taxReceipt.dao.Shopping;
import br.taxReceipt.dao.ShoppingImpl;

@Controller
public class AppController {

	Shopping shopping = new ShoppingImpl();
	
    @RequestMapping({"/","/home"})
    public String home() {
        return "home";
    }
	
    @RequestMapping("/input")
    public String input(@RequestParam(value="inputId", required=true, defaultValue="1") String inputId, Model model) {
    	ShoppingBasket basket = (ShoppingBasket) shopping.find(Integer.parseInt(inputId));
        model.addAttribute("inputId", inputId);
        model.addAttribute("products", basket.getProducts());
        return "input";
    }
    
    @RequestMapping("/output")
    public String output(@RequestParam(value="inputId", required=true, defaultValue="1") String inputId, Model model) {
    	ShoppingBasket basket = (ShoppingBasket) shopping.find(Integer.parseInt(inputId));   	
        model.addAttribute("inputId", inputId);
        model.addAttribute("products", basket.getProducts());
        model.addAttribute("salesTaxes", basket.getTotalTaxValueFormated());
        model.addAttribute("total", basket.getTotalValueFormated());
        return "output";
    }
    
    @ExceptionHandler
    public String error(@RequestParam(value="inputId", required=true, defaultValue="1") String inputId, Model model) {
    	return "error";
    }

}