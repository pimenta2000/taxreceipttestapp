package test.java;

import static org.junit.Assert.assertTrue;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Locale;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.mock.web.MockServletContext;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import br.taxReceipt.bo.ShoppingBasket;
import br.taxReceipt.model.Book;
import br.taxReceipt.model.Food;
import br.taxReceipt.model.MedicalProduct;
import br.taxReceipt.model.Product;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = MockServletContext.class)
public class ProductTest {
	
	private static boolean IMPORTED_PRODUCT = true;

	@Test
	public void testMusicCD(){
		Product product = new Product("music cd", 1, 14.99);
		assertTrue(product.getSalesPriceFormatted().equals("16.49"));
	}

	@Test
	public void testBook(){
		Product book = new Book("book", 1, 12.49);
		assertTrue(book.getSalesPriceFormatted().equals("12.49"));
	}

	@Test
	public void testFood(){
		Product chocolateBar = new Food("chocolate bar", 1, 0.85);
		assertTrue(chocolateBar.getSalesPriceFormatted().equals("0.85"));
	}	
	
	
	@Test
	public void testTotal(){
		ShoppingBasket basket = new ShoppingBasket();
		basket.setInputId(1);
		basket.insert(new Product("music cd", 1, 14.99));
		basket.insert(new Book("book", 1, 12.49));
		basket.insert(new Food("chocolate bar", 1, 0.85));
		String df = new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.US)).format(basket.getTotalValue());
		assertTrue(df.equals("29.83"));
	}	
	
	
	@Test
	public void testReceiptTotalTax(){
		ShoppingBasket basket = new ShoppingBasket();
		basket.setInputId(1);
		basket.insert(new Product("music cd", 1, 14.99));
		basket.insert(new Book("book", 1, 12.49));
		basket.insert(new Food("chocolate bar", 1, 0.85));
		String df = new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.US)).format(basket.getTotalTaxValue());
		assertTrue(df.equals("1.50"));
	}	
	

	@Test
	public void testImportedFood(){
		Product chocolateBar = new Food("imported box of chocolates", 1, 10.00, IMPORTED_PRODUCT);
		assertTrue(chocolateBar.getSalesPriceFormatted().equals("10.50"));
	}	

	@Test
	public void testImportedBottleOfPerfume(){
		Product perfume = new Product("imported bottle of perfume", 1, 47.50, IMPORTED_PRODUCT);
		assertTrue(perfume.getSalesPriceFormatted().equals("54.65"));
	}	
	
	@Test
	public void testTotalTax2(){
		ShoppingBasket basket = new ShoppingBasket();
		basket.setInputId(2);
		basket.insert(new Food("imported box of chocolates", 1, 10.00, IMPORTED_PRODUCT));
		basket.insert(new Product("imported bottle of perfume", 1, 47.50, IMPORTED_PRODUCT));
		String df = new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.US)).format(basket.getTotalTaxValue());
		assertTrue(df.equals("7.65"));
	}	
	
	@Test
	public void testTotal2(){
		ShoppingBasket basket = new ShoppingBasket();
		basket.setInputId(2);
		basket.insert(new Food("imported box of chocolates", 1, 10.00, IMPORTED_PRODUCT));
		basket.insert(new Product("imported bottle of perfume", 1, 47.50, IMPORTED_PRODUCT));
		String df = new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.US)).format(basket.getTotalValue());
		assertTrue(df.equals("65.15"));
	}	
	
	@Test
	public void testImportedBottleOfPerfume2(){
		Product perfume = new Product("imported bottle of perfume", 1, 27.99, IMPORTED_PRODUCT);
		assertTrue(perfume.getSalesPriceFormatted().equals("32.19"));
	}	

	@Test
	public void testBottleOfPerfume(){
		Product perfume2 = new Product("bottle of perfume", 1, 18.99);
		assertTrue(perfume2.getSalesPriceFormatted().equals("20.89"));
	}

	@Test
	public void testHeadachePills(){
		Product pills = new MedicalProduct("packet of headache pills", 1, 9.75);
		assertTrue(pills.getSalesPriceFormatted().equals("9.75"));
	}
	
	@Test
	public void testImportedFood2(){
		Product chocolateBar = new Food("imported box of chocolates", 1, 11.25, IMPORTED_PRODUCT);
		assertTrue(chocolateBar.getSalesPriceFormatted().equals("11.85"));
	}	
	
	@Test
	public void testTotalTax3(){
		ShoppingBasket basket = new ShoppingBasket();
		basket.setInputId(3);
		basket.insert(new Product("imported bottle of perfume", 1, 27.99, IMPORTED_PRODUCT));
		basket.insert(new Product("bottle of perfume", 1, 18.99));
		basket.insert(new MedicalProduct("packet of headache pills", 1, 9.75));
		basket.insert(new Food("imported box of chocolates", 1, 11.25, IMPORTED_PRODUCT));
		String df = new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.US)).format(basket.getTotalTaxValue());
		assertTrue(df.equals("6.70"));
	}
	
	@Test
	public void testTotal3(){
		ShoppingBasket basket = new ShoppingBasket();
		basket.setInputId(3);
		basket.insert(new Product("imported bottle of perfume", 1, 27.99, IMPORTED_PRODUCT));
		basket.insert(new Product("bottle of perfume", 1, 18.99));
		basket.insert(new MedicalProduct("packet of headache pills", 1, 9.75));
		basket.insert(new Food("imported box of chocolates", 1, 11.25, IMPORTED_PRODUCT));
		String df = new DecimalFormat("#0.00", new DecimalFormatSymbols(Locale.US)).format(basket.getTotalValue());
		assertTrue(df.equals("74.68"));
	}
	
}
